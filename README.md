# README #

Para executar esse projeto basta realizar o clone do repositorio e abrir o projeto utilizando a IDE Delphi
Esse projeto foi desenvolvido para exemplificar uma maneira de criar diferentes tamanhos de etiqueta para
os drivers da Elgin L42 e Elgin L42Pro utilizando os registros do Windows.
Também é possivel selecionar o papel a ser utilizado pelo driver.

Contem contribuições de Paulo Gervilla e Bruno Cruz

* Versão - 1.0.0
* S.O - Windows 10 64bits

### Como configurar? ###

* Instruções pré compilação  
	Antes de executar este exemplo é necessário que os drivers de impressão
	para as impressoras Elgin L42 e L42Pro estejam instalados.  
	As versões usadas no momento do desenvolvimento foram:  
	- L42  
		- L42_BR V2.08  
	- L42Pro  
		- Produto:	ELGIN EPL Driver  
		- Versão:	2019.1  
		
* Esse exemplo altera registros do windows referente aos dados da impressora l42 e l42pro.   
  Desse modo é recomendado que o mesmo seja executado em maquina virtual para evitar problemas com o sistema operacional    

* Registros alterados  
	- HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Print\Forms  
	- HKEY_LOCAL_MACHINE\SOFTWARE\Seagull Scientific\Drivers\Paper Sizes  
	- HKEY_CURRENT_USER\Printers\DevModePerUser  
	- HKEY_CURRENT_USER\Printers\DevModes2  

		
### Achou um problema ###

* [issues](https://bitbucket.org/teamshelgin/l42-l42pro-cria-tamanho-papel/issues?status=new&status=open)