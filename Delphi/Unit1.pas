unit Unit1;

interface

uses
  Vcl.StdCtrls, Vcl.ExtCtrls, System.Classes, Vcl.Controls, Vcl.Forms, Printers,
  Dialogs, Registry, SysUtils, Windows, ShellApi;


type
  TForm1 = class(TForm)
    edtAdobeReader: TLabeledEdit;
    btnImprimePDF: TButton;
    btnCriaPapeis: TButton;
    edtArquivoPDF: TLabeledEdit;
    GroupBox1: TGroupBox;
    btnSeleciona40x30: TButton;
    btnSeleciona80x30: TButton;
    GroupBox2: TGroupBox;
    btnSeleciona80x120: TButton;
    btnSeleciona40x30_Pro: TButton;
    btnSeleciona80x30_Pro: TButton;
    btnSeleciona80x120_Pro: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnCriaPapeisClick(Sender: TObject);
    procedure btnSeleciona80x30Click(Sender: TObject);
    procedure btnSeleciona40x30Click(Sender: TObject);
    procedure btnImprimePDFClick(Sender: TObject);
    procedure btnSeleciona80x120Click(Sender: TObject);
    procedure btnSeleciona40x30_ProClick(Sender: TObject);
    procedure btnSeleciona80x30_ProClick(Sender: TObject);
    procedure btnSeleciona80x120_ProClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses UnitPrinterConfig;

procedure TForm1.btnImprimePDFClick(Sender: TObject);
begin
  ShellExecute(
    Handle,
    'open',
    PWideChar(edtAdobeReader.Text + '\AcroRd32.exe'),
    PWideChar('/t "' + ExtractFileDir(Application.ExeName) + '\' + edtArquivoPDF.Text + '" "BTP-L42(U)"'),
    PWideChar(edtAdobeReader.Text),
    SW_SHOWNORMAL);
end;

procedure TForm1.btnCriaPapeisClick(Sender: TObject);
var
{    -------------------- relacionado a cria��o no Registro
   Registry: TRegistry;
   buffer: TBytes;
   size, iaux: Integer;
   straux: String;
}
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.CriaPapeis;
  if( ret ) then
    ShowMessage('� necess�rio reiniciar o computador.')
  else
    ShowMessage('Falha ao criar os tamanhos de papel');

  FreeAndNil(L42);
 {   -------------------- relacionado a cria��o no Registro
   Registry := TRegistry.Create(KEY_ALL_ACCESS);
   try
     Registry.RootKey := HKEY_LOCAL_MACHINE;


     // False because we do not want to create it if it doesn't exist
     if( Registry.OpenKey('SYSTEM\CurrentControlSet\Control\Print\Forms', True) ) then
     begin
       if( not Registry.ValueExists('BTP-L42(U)') ) then
       begin

       end;

       size := Registry.GetDataSize('BTP-L42(U)');
       SetLength(buffer, size);

       Registry.ReadBinaryData('BTP-L42(U)', Pointer(buffer)^, size);
     end;
   finally
     Registry.Free;
   end;
}
end;

procedure TForm1.btnSeleciona40x30Click(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.Seleciona_40x30;
  if( ret ) then
    ShowMessage('Selecionado com sucesso.');

  FreeAndNil(L42);
end;

procedure TForm1.btnSeleciona40x30_ProClick(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.Seleciona_40x30_PRO;
  if( ret ) then
    ShowMessage('Selecionado com sucesso.');

  FreeAndNil(L42);

end;

procedure TForm1.btnSeleciona80x120Click(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.Seleciona_80x120;
  if( ret ) then
    ShowMessage('Selecionado com sucesso.');

  FreeAndNil(L42);
end;

procedure TForm1.btnSeleciona80x120_ProClick(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.Seleciona_80x120_PRO;
  if( ret ) then
    ShowMessage('Selecionado com sucesso.');

  FreeAndNil(L42);

end;

procedure TForm1.btnSeleciona80x30Click(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.Seleciona_80x30;
  if( ret ) then
    ShowMessage('Selecionado com sucesso.');

  FreeAndNil(L42);
end;

procedure TForm1.btnSeleciona80x30_ProClick(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
begin
  L42 := TL42PrinterConfig.Create;

  ret := L42.Seleciona_80x30_PRO;
  if( ret ) then
    ShowMessage('Selecionado com sucesso.');

  FreeAndNil(L42);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  L42: TL42PrinterConfig;
  ret: Boolean;
  begin

  if(ParamCount > 0) then
  begin

    try
      if ParamStr(1) = '-c' then
      begin
        L42 := TL42PrinterConfig.Create;
        ret := L42.CriaPapeis;

        if ParamCount > 1 then
        begin
           if ParamStr(2) = '-d' then
           begin
              AllocConsole;
              if( ret ) then
              begin
                Writeln('Sucesso! Necess�rio reiniciar o computador.');
              end
              else
              begin
                Writeln('Falha ao criar os tamanhos de papel');
              end;
           end;
           Writeln('Enter to continue.');
           ReadLn;
        end;
        FreeAndNil(L42);
      end
      else if ParamStr(1) = '-h' then
      begin
        AllocConsole;
        Writeln('Uso: ' +  ExtractFileName(Application.ExeName) + '[-options]');
        writeln('-c     Cria tamanho diferentes de papel para a impressora L42 e L42PRO[40x30 80x30 80x120]');
        writeln('-d     Informa status da cria��o dos papeis');
        writeln('-h     Ajuda');
        Writeln('Pressione ENTER para continuar.');
        ReadLn;
      end
      else if ParamStr(1) = '-d' then
      begin
        AllocConsole;
        Writeln('Nenhum opera��o foi executada.');
        Writeln('Tente passar o parametro -c seguido de -d para ter o debug da operacao!');
        Writeln('Pressione ENTER para continuar.');
        ReadLn;
      end;

    finally
      FreeConsole;
      Application.Terminate;
    end;

  end;


  {
  ESSA IMPLEMENTA��O FOI REESCRITA PARA QUE N�O INVALIDASSE O USO DO PROGRAMA PELA INTERFACE
  E AS SAIDAS FORAM CONFIGURADAS PARA SEREM GERADAS NA CONSOLE
  TBM FOI ADICIONADO AS OP��ES -d PARA DEBUG E -h PARA AJUDA
    straux := '';

    for iaux := 0 to ParamCount do
      straux := straux + AnsiString(ParamStr(iaux)) + ' ';
      analisa:= copy(straux, Length(straux)-7, 7);

      if(analisa = '.exe -c') then
      begin
          L42 := TL42PrinterConfig.Create;
          ret := L42.CriaPapeis;

        if( ret ) then
        begin
          ShowMessage('Sucesso! Necess�rio reiniciar o computador.');
        end
        else begin
          ShowMessage('Falha ao criar os tamanhos de papel');
          FreeAndNil(L42);
        end;
      end;

      if(analisa <> '.exe -c') then
      begin
        ShowMessage('Falha! Par�metro inserido � inv�lido');
      end;

    //if( Length(straux) > 0 ) then ShowMessage(Trim(String(straux)));
    //if( Length(straux) > 0 ) then ShowMessage(Trim(String(analisa)));
    Application.Terminate;
    }
end;

end.
