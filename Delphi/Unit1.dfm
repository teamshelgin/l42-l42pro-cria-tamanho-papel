object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Form1'
  ClientHeight = 353
  ClientWidth = 339
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object edtAdobeReader: TLabeledEdit
    Left = 8
    Top = 254
    Width = 305
    Height = 21
    EditLabel.Width = 193
    EditLabel.Height = 13
    EditLabel.Caption = 'Caminho Adobe Reader (AcroRd32.exe)'
    TabOrder = 0
    Text = 'C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader'
  end
  object btnImprimePDF: TButton
    Left = 8
    Top = 320
    Width = 305
    Height = 25
    Caption = 'Imprimir PDF'
    TabOrder = 1
    OnClick = btnImprimePDFClick
  end
  object btnCriaPapeis: TButton
    Left = 8
    Top = 8
    Width = 313
    Height = 25
    Caption = 'Cria Tamanhos de Papel'
    TabOrder = 2
    OnClick = btnCriaPapeisClick
  end
  object edtArquivoPDF: TLabeledEdit
    Left = 8
    Top = 293
    Width = 305
    Height = 21
    EditLabel.Width = 121
    EditLabel.Height = 13
    EditLabel.Caption = 'Caminho do arquivo PDF:'
    TabOrder = 3
    Text = 'etiqueta (Medicamentos).pdf'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 39
    Width = 313
    Height = 90
    Caption = 'L42'
    TabOrder = 4
    object btnSeleciona40x30: TButton
      Left = 3
      Top = 20
      Width = 150
      Height = 25
      Caption = 'Seleciona 40x30mm'
      TabOrder = 0
      OnClick = btnSeleciona40x30Click
    end
    object btnSeleciona80x30: TButton
      Left = 159
      Top = 20
      Width = 150
      Height = 25
      Caption = 'Seleciona 80x30mm'
      TabOrder = 1
      OnClick = btnSeleciona80x30Click
    end
    object btnSeleciona80x120: TButton
      Left = 3
      Top = 51
      Width = 150
      Height = 25
      Caption = 'Seleciona 80x120mm'
      TabOrder = 2
      OnClick = btnSeleciona80x120Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 141
    Width = 313
    Height = 84
    Caption = 'L42-Pro'
    TabOrder = 5
    object btnSeleciona40x30_Pro: TButton
      Left = 3
      Top = 16
      Width = 150
      Height = 25
      Caption = 'Seleciona 40x30mm'
      TabOrder = 0
      OnClick = btnSeleciona40x30_ProClick
    end
    object btnSeleciona80x30_Pro: TButton
      Left = 159
      Top = 16
      Width = 151
      Height = 25
      Caption = 'Seleciona 80x30mm'
      TabOrder = 1
      OnClick = btnSeleciona80x30_ProClick
    end
    object btnSeleciona80x120_Pro: TButton
      Left = 3
      Top = 47
      Width = 150
      Height = 25
      Caption = 'Seleciona 80x120mm'
      TabOrder = 2
      OnClick = btnSeleciona80x120_ProClick
    end
  end
end
